def simple_key(*args, **kwargs):
	""" Create a simple cache key.
	Can handle any arguments.
	"""

	params = [str(x) for x in args]
	params.extend(['{0}={1}'.format(name, value) for name, value in kwargs.items()])
	return ', '.join(params)

def cached(cache = None, key_gen = simple_key):
	""" Creates a caching decorator.
	Keyword arguments:
	cache -- dictionary-like object to be used as a cache store
	key_gen -- function to generate a cache key from the wrapped function arguments
	"""

	if cache is None:
		cache = dict()
	
	def cached_decorator(fn):
		def wrapper(*args, **kwargs):
			key = key_gen(*args, **kwargs)
			value = None
			
			if key in cache.keys():
				value = cache[key]
			else:
				value = fn(*args)
				cache[key] = value
			
			return value

		return wrapper
	
	return cached_decorator
