import cached
import unittest

class TestCached(unittest.TestCase):

	def test_simple_key(self):
		key = cached.simple_key(1, 'a', 5)
		self.assertEqual(key, '1, a, 5')

		key = cached.simple_key(param1='a', param2='b')
		self.assertEqual(key, 'param1=a, param2=b')

		key = cached.simple_key('a', 2, param1='z', param2=3)
		self.assertEqual(key, 'a, 2, param1=z, param2=3')

	def test_cached_simple(self):
		@cached.cached()
		def foo(user_id):
			self._foo_called = True
			return user_id

		self._foo_called = False
		id = foo(5)
		self.assertEqual(id, 5)
		self.assertTrue(self._foo_called)

		self._foo_called = False
		id = foo(6)
		self.assertEqual(id, 6)
		self.assertTrue(self._foo_called)

		self._foo_called = False
		id = foo(5)
		self.assertEqual(id, 5)
		self.assertFalse(self._foo_called)

	def test_cached_advanced(self):
		cache = dict()
		
		def get_key(id):
			return 'id -> {0}'.format(id)

		@cached.cached(cache=cache, key_gen=get_key)
		def foo(user_id):
			return user_id

		value = 5

		foo(value)
		cached_value = cache[get_key(value)]
		self.assertEqual(cached_value, value)

if __name__ == '__main__':
	unittest.main()