""" dots_and_spaces module tests
"""

import unittest
import dots_and_spaces

class TestDotsAndSpaces(unittest.TestCase):
    """ dots_and_spaces module tests
    """

    def test_extract_numbers(self):
        """ Test numbers extraction
        """
        nums = dots_and_spaces.extract_numbers('1,2.3;m4')
        self.assertEqual(nums, ['1', '2', '3', '4'])

    def test_bit(self):
        """ Test bit check
        """
        bit = dots_and_spaces.bit(4, 0)
        self.assertEqual(bit, 0)

        bit = dots_and_spaces.bit(4, 2)
        self.assertEqual(bit, 1)

        bit = dots_and_spaces.bit(4, 5)
        self.assertEqual(bit, 0)

    def test_stuff_with_dots_and_spaces(self):
        """ Test how numbers are stuffed with dots'n'spaces
        """
        numbers = ['1', '2', '3']

        result = dots_and_spaces.stuff_with_dots_and_spaces(numbers, 2)
        self.assertEqual(result, '1 2.3')

        result = dots_and_spaces.stuff_with_dots_and_spaces(numbers, 3)
        self.assertEqual(result, '1.2.3')

    def test_dots_and_spaces(self):
        """ Test dots_and_spaces function
        """
        result = dots_and_spaces.dots_and_spaces('1,2,3')

        self.assertEqual(len(result), 4)
        for option in ['1 2 3', '1.2 3', '1 2.3', '1.2.3']:
            self.assertTrue(option in result)

if __name__ == '__main__':
    unittest.main()
