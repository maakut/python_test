""" Task 3
"""

import re

_NUMBERS_ = re.compile(r'(\d+)')

def extract_numbers(string):
    """ Extracts all numbers from the string provided
    """
    return _NUMBERS_.findall(string)

def bit(number, position):
    """ Gets bit from the specified number
    Arguments:
    number - number to get bits from
    position - bit position
    """
    return ((1 << position) & number) >> position

def stuff_with_dots_and_spaces(numbers, mask):
    """ Returns string with numbers, separated by dots and spaces according to the mask
    """
    count = len(numbers)
    index = 0
    result = ''

    for num in numbers:
        result += num
        if index < count - 1:
            result += '.' if bit(mask, index) == 1 else ' '
            index += 1

    return result

def dots_and_spaces(string):
    """ Returns array of strings, each containing numbers from the string provided
    separated by dots and spaces
    """
    numbers = extract_numbers(string)
    count = len(numbers)
    top = 1 << (count - 1)
    return [stuff_with_dots_and_spaces(numbers, x) for x in range(0, top)]
